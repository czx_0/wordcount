#!/usr/bin/python
import mechanicalsoup
import re


def crawlWebsite(url):
    print("Crawling website ... ")
    # List of links to crawl
    browser = mechanicalsoup.StatefulBrowser()
    browser.open(url)

    # Get all links in page
    links = browser.get_current_page().find_all('a')
    uep = [ x.get('href') for x in links ]

    # Check if links belongs to domain?

    # Excluse hrefs with #

    return links

def googleCrawl(url):
    # Use google search with "site" to get all target links to process.
    print("Conecting to google ...")

    browser = mechanicalsoup.StatefulBrowser()
    try:
        browser.open("https://google.com")
    
        # Google Search with "site"
        browser.select_form('form[action="/search"]')
        browser["q"] = "site:"+url
        browser.submit_selected(btnName="btnG")
    except Exception as e:
        print("%s" % e)
        exit(1)

    # Get target links, filter-out unrelated links
    targets = []
    for link in browser.links():
        target = link.attrs['href']
        if(target.startswith('/url?') and not target.startswith("/url?q=http://webcache.googleusercontent.com")):
            target = re.sub(r"^/url\?q=([^&]*)&.*", r"\1", target)
            targets.append(target)

    return targets

def parseWebsite(url):
    print("Parsing website: %s  ..." % url)
    browser = mechanicalsoup.StatefulBrowser()
    try:
        # Open target link
        browser.open(url) 

        # Get only text from website
        page = browser.get_current_page().get_text()
    except Exception as e:
        print("%s" % e)
        page = ""

    return page

def countWords(word, parsedwebsite):
    print("Searching and counting word ... ")

    # Word count by spliting when word found, since 1 word = 2 items on list, we subtract 1 at the end.
    words = len(parsedwebsite.lower().split(word.lower())) - 1 

    return words


