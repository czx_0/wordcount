FROM python:3.8

RUN pip install mechanicalsoup docopt

WORKDIR /root/

COPY wordcount.py .
COPY wordLib.py .

RUN chmod +x wordcount.py wordLib.py

ENTRYPOINT [ "/root/wordcount.py" ]
