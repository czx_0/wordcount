### Word Count

This script counts how many times the word **--word** is found on the website **--url** and prints it. 

By default, it only scans the **--url** argument. With **--crawl** flag, it'll use a google search to look for target links on the same site.


### TODO
- Code crawler (crawlWebsite) to choose google crawl or this one
