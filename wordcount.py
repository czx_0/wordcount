#!/usr/local/bin/python
from  wordLib import *
from docopt import docopt

def main():
    usage = """Usage:
        wordcount -h | --help
        wordcount --word <word> --url <url> [--crawl]

    Options:
        -h --help           Show this screen
        --word              Word to search for
        --url               Website where the word will be search
        --crawl             Crawling with google, optional
        -v --verbose        Verbose
    """
    arguments = docopt(usage, version=None)
   
    if arguments['--word'] and arguments['--url']:
        if arguments['--crawl']:
            targets = googleCrawl(arguments['<url>'])
        else: 
            targets = [arguments['<url>']]
        
        for link in targets:
            parsedwebsite = parseWebsite(link)
            count = countWords(arguments['<word>'], parsedwebsite)
            print("%s: %s\n" % (link,count))

if __name__ == '__main__':
    main()
